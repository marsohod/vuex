import axios from 'axios'
import api from '@/api/modules/users'
import apiAuth from '@/api/modules/auth'
import {STORAGE_TOKEN, STORAGE_REFRESH_TOKEN} from '@/api/apiTypes'
import {User, USER_STATUSES} from '@/models/user'
import _ from 'lodash'
import * as types from '../types'
import moment from 'moment'

const state = _.defaults({
	me 		: null,
	items 	: [],
	current : null,
	statuses: USER_STATUSES,
	total 	: null
}, types.state);

const getters = _.defaults({
	getItems: (state, getters, rootState ) => {
		return state.items.map( (v, i) => {
			return new User(v);
		} );	
	},

	getById: (state, getters, rootState) => (id) => {
		if ( getters.getItems.length == 0 ) { return null; }

		return _.filter( getters.getItems, (v) => v.id == id );
	},

	getByUsername: (state, getters, rootState) => (name) => {
		if ( getters.getItems.length == 0 ) { return null; }

		return _.filter( getters.getItems, (v) => v.username == name );
	},
	getCurrent: (state) => state.current,
	getTotal: (state) => state.total,
	getItemsForSelect: (state) => {
		return state.items.map( (v, i) => ({value: v.id, text: _.isNil(v.username) ? v.login : v.username}))
	},

	isLogged: (state) => _.isNil(state.me) ? false : state.me,
}, types.getters);

const actions = {

/* START AUTH */
	async login ({ commit }, data ) {
		commit(types.START_REQUEST);
		console.log('Auth', data);

		try {
			let res = await apiAuth.auth( data );

			localStorage.setItem(STORAGE_TOKEN, res.data.token);
			localStorage.setItem(STORAGE_REFRESH_TOKEN, res.data.refresh_token);

			let me 	= await apiAuth.me();

			commit('logged', me.data); 
			commit(types.PENDING, false);
		} catch (err) {
			commit(types.FAILURE, err);
			localStorage.clear(STORAGE_TOKEN);
			localStorage.clear(STORAGE_REFRESH_TOKEN);
			return new Promise.reject( types.parseError(err) );
		}
	},

	async logout ({ commit }) {
		commit(types.START_REQUEST);
		console.log('Logout');

		localStorage.clear(STORAGE_TOKEN);
		localStorage.clear(STORAGE_REFRESH_TOKEN);

		commit('logged', null);

		try {
			await apiAuth.logout();
			commit(types.PENDING, false);
		} catch ( err ) {
			commit(types.PENDING, false);
		}
	},

	async remember ({ commit }, data) {
		commit(types.START_REQUEST);
		console.log('Remember password');

		try {
			let a = await apiAuth.remember( data );
			commit(types.PENDING, false);
		} catch ( err ) {
			commit(types.PENDING, false);
			commit(types.FAILURE, err);
			return new Promise.reject( types.parseError(err) );
		}
	},

	async signup ({ commit }, data ) {
		commit(types.START_REQUEST);
		console.log('Sign Up', data);

		try {
			await apiAuth.signup( data );
			return this.dispatch('users/login');
		} catch (err) {
			commit(types.PENDING, false);
			commit(types.FAILURE, err);
			return new Promise.reject( err );
		}
	},

	async me ({ commit }, data ) {
		commit(types.START_REQUEST);
		console.log('getMe', data);

		try {
			let me 	= await apiAuth.me();
			commit('logged', me.data); 
			commit(types.PENDING, false);
		} catch (err) {
			commit(types.FAILURE, err);
			localStorage.clear(STORAGE_TOKEN);
			localStorage.clear(STORAGE_REFRESH_TOKEN);
			return new Promise.reject( err );
		}
	},

/* END AUTH */

/* START USERS */

	async getItems ({ commit }, data = null ) {
		commit(types.START_REQUEST);
		console.log('getUsers', data);

		try {
			let res = await api.getItems(data);	
			commit('setItems', res.data['hydra:member']);
			commit('setTotal', res.data['hydra:totalItems']);
			commit(types.PENDING, false);
		} catch (err) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
		}
	},

	async getOne ({ commit }, data = null ) {
		commit(types.START_REQUEST);
		console.log('getUser', data);

		try {
			let res = await api.getItems(data);	

			if ( res.data['hydra:member'].length == 0 ) {
				return Promise.reject( 'Unexist user' );
			}

			commit('setCurrent', res.data['hydra:member'][0]);
			commit(types.PENDING, false);
			return new User(res.data['hydra:member'][0]);
		} catch (err) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	},

	async put ( {commit}, data ) {
		commit(types.START_REQUEST);
		console.log('pushUser', data);

		try {
			let res = await api.putItem(data);	
			commit('push', res.data);
			commit(types.PENDING, false);
			return new User(res.data);
		} catch ( err ) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	}
}

const mutations = _.defaults({
	logged ( state, v ) {
		state.me = new User(v);
	},
	setItems ( state, items ) {
		state.items = items;
	},
	setTotal ( state, v ) {
		state.total = v;
	},
	setCurrent ( state, v ) {
		state.current = new User(v);
	},
	clearCurrent ( state, name ) {
		if ( state.current != null && state.current.username != name ) {
			state.current = null;
		}
	},
	push ( state, v ) {
		state.items.push(v);
	}
}, types.mutations);

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}