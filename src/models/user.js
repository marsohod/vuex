export class User {
	constructor ( data ) {
		if ( _.isNil(data) ) { return }
		
		_.assignIn(this, {
			id					: data.id,
			created_at			: data.created_at,
			registeredAt		: data.registeredAt,
			loggedAt			: data.logged_at,
			avatar 				: '/static/img/avatars/1.png',
			email 				: data.email,
			username 			: data.username,
			fullname 			: data.fullname == null ? 'Leonid Pavlovich' : data.fullname,
			bio 				: data.bio,
			role 				: data.role,
			status 				: data.status,
			isEnabled			: data.status != 0,
			ideas				: data.ideas ? data.ideas.total : null,
			socials				: data.socials ? data.socials : [],
			subscription 		: null
		});
	}

	isAdmin () {
		return this.role.some( v => v === USER_ROLES[0].id );
	}
}

export const USER_STATUSES = [
	{
		id 		: 'ACTIVE',
		name 	: 'Active'
	}, 
	{
		id 		: 'verified',
		name 	: 'Verified'
	},
	{
		id 		: 'paid',
		name 	: 'Paid'
	},
	{
		id 		: 'DELETED',
		name 	: 'Deleted'
	}, ];

const USER_ROLES = [
	{
		id 		: 'admin',
		group 	: 'admins'
	}, {
		id 		: 'developer',
		group 	: 'admins'
	}, {
		id 		: 'user',
		group 	: 'users'
	}];