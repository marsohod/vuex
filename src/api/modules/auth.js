import axios from 'axios'
import * as api from '../apiTypes'

const URL = api.URL + 'auth';

export default {
	auth ( data ) {
		return new Promise( resolve => {
			setTimeout( () => {
				resolve({ data: {token	: 'asdf', refresh_token: "gfgfg"}});
			}, 500);
		})
	},

	logout ( data ) {
		return new Promise( resolve => {
			setTimeout( () => {
				resolve();
			}, 100);
		})
	},

	remember ( data ) {
		return new Promise( (resolve, reject) => {
			setTimeout( () => {
				resolve();
			}, 100);
		})
	},

	signup ( data ) {
		return new Promise( (resolve, reject) => {
			setTimeout( () => {
				resolve();
			}, 100);
		})
	},

	me () {
		return new Promise( (resolve, reject) => {
			setTimeout( () => {
				resolve({
					data : {
						id 			: 4,
						avatar 		: '/static/img/avatars/1.png',
						username 	: '2themoon-stop-as-target',
						fullname 	: 'Abramov Alexander',
						registeredAt: '2018-01-01',
						bio 		: 'Everyone has the same problems when starting to make money on crypto. No experience,  lots of news and controversial information.',
					}
				});
			}, 500);
		})
	}
}