import mixins from './mixins/index.js';
import { Bar, HorizontalBar, Doughnut, Line, Pie, PolarArea, Radar, Bubble, Scatter } from './BaseCharts';
var VueCharts = {
  Bar: Bar,
  HorizontalBar: HorizontalBar,
  Doughnut: Doughnut,
  Line: Line,
  Pie: Pie,
  PolarArea: PolarArea,
  Radar: Radar,
  Bubble: Bubble,
  Scatter: Scatter,
  mixins: mixins
};
export default VueCharts;
export { VueCharts, Bar, HorizontalBar, Doughnut, Line, Pie, PolarArea, Radar, Bubble, Scatter, mixins };