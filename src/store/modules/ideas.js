import {Idea, IDEA_STATUSES} from '@/models/idea'
import api from '@/api/modules/ideas'
import apiComments from '@/api/modules/comments'
import _ from 'lodash'
import * as types from '../types'
import { Term } from '../../models/term';
import { Comment } from '../../models/comment'

export const START_REQUEST_PUSH_COMMENT 	= 'START_REQUEST_PUSH_COMMENT';

function preparePerformance ( items ) {
	let out = [];
	let months = {};

	_.each(items, (v, i) => {
		_.each(v, (vv, j) => {
			if ( !months[j] ) {
				months[j] = {date: j};
			}

			months[j][i.toLowerCase()] = vv;
			months[j].projectname = null;
		});
	});
	
	for ( let i in months ) {
		out.push(months[i]); 
	}
	
	return out;
}

const state = _.defaults({
	items 				: [],
	results				: [],
	performance 		: [],
	indexstat			: [],
	opened				: null,
	statuses			: IDEA_STATUSES,
	total 				: null,
	pendingPushComment 	: false
}, types.state);

const getters = _.defaults({
	getItems: (state, getters, rootState ) => {
		return state.items;
	},

	getResults: (state, getters, rootState ) => {
		return state.results;
	},

	getPerformance: (state, getters, rootState ) => {
		return state.performance;
	},

	getIndexStat: (state, getters, rootState ) => {
		return state.indexstat;
	},

	getCommentsById: (state) => (id) => {
		if ( getters.getItems.length == 0 ) { return []; }		
		
		let list = _.filter( state.items, (v) => v.id == id);

		if ( list.length == 0 ) { return []; }
	},

	opened: (state) => {
		return state.opened;
	},

	openedComments: () => {
		if ( !state.opened ) { return []; }

		return state.opened.comments;
	},

	isPendingPushComment: state => state.pendingPushComment,

	getTotal: (state) => state.total,
}, types.getters);

const actions = {
	async getItems ({ commit }, data ) {
		commit(types.START_REQUEST);
		console.log('getIdeas', data);

		try {
			let res = await api.getItems(data);
			commit('setItems', res.data['hydra:member']);
			commit('setTotal', res.data['hydra:totalItems']);
			commit(types.PENDING, false);
		} catch ( err ) {
			// debugger;
			commit(types.FAILURE, err.response.data);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	},

	async getById ({ commit, getters, state }, id ) {
		commit(types.START_REQUEST);
		console.log('getIdeaById', id)
		
		try {
			let res = await api.getById(id);
			commit('setOpened', res.data );
			commit('updateIdea', res.data );
			commit(types.PENDING, false);
			return new Idea(res.data);
		} catch ( err ) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	},

	async put ( {commit}, data ) {
		commit(types.START_REQUEST);
		console.log('pushIdea', data);

		try {
			let res = await api.putItem(data);	
			commit('push', res.data);
			commit(types.PENDING, false);
			return new Idea(res.data);
		} catch ( err ) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	},

	async patch ( {commit}, data ) {
		commit(types.START_REQUEST);
		console.log('editIdea', data);

		try {
			let res = await api.patchItem(data);
			commit('updateIdea', res.data);
			commit(types.PENDING, false);
			return new Idea(res.data);
		} catch ( err ) {
			commit(types.FAILURE, err);
			commit(types.PENDING, false);
			return Promise.reject( types.parseError(err) );
		}
	}
};

const mutations = _.defaults({
	setItems ( state, items ) {		
		state.items = items.map( (v) => new Idea(v) );
	},

	setOpened ( state, v ) {
		state.opened = v == null ? v : new Idea(v);
	},

	setRates ( state, items ) {
		let max = _.maxBy(items, v => v.close );
		let min = _.minBy(items, v => v.close );

		state.opened.rates = items.map( v => {
			if ( v.close == max.close ) v.isMax = true;
			if ( v.close == min.close ) v.isMin = true;

			return v;
		});
	},

	setTotal ( state, v ) {
		state.total = v;
	},

	push ( state, v ) {
		state.items.push( new Idea(v) );
	},

	updateIdea ( state, item ) {
		let index = _.findIndex(state.items, (i) => i.id == item.id);
		
		if ( index == - 1  || state.items.length <= index ) {
			return;
		}

		state.items = state.items.slice(0, index).concat( item instanceof Idea ? item : new Idea(item), state.items.slice(index+1));
	}
}, types.mutations);

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};