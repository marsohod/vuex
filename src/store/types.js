export const PENDING 		= 'PENDING';
export const FAILURE 		= 'FAILURE';
export const START_REQUEST 	= 'START_REQUEST';

export const state 			= {
	pending : false,
	failure : false,
	statuses: []
}

export const getters 		= {
	isPending: state => state.pending,
	isFailure: state => state.failure,
	getStatusesForSelect: (state) => state.statuses.map( (v, i) => ({value: v.id, text: v.name}))
}

export const mutations 		= {
	[PENDING] ( state, value ) {
		state.pending = value;
	},
	[FAILURE] ( state, value ) {
		state.failure = value;
	},
	[START_REQUEST] ( state ) {
		state.pending = true;
		state.failure = false;
	}
}

export function parseError ( err ) {
	if ( _.isObject(err) && _.isObject(err.response) && _.isObject(err.response.data) ) { return err.response.data.detail; }
	return err.toString();
}

/*const asyncMutations = (type) => ({
	SUCCESS : `#{type}_SUCCESS`,
	FAILURE : `#{type}_FAILURE`,
	PENDING : `#{type}_PENDING`
})*/