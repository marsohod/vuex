import axios from 'axios'
import apiMarkets from '@/api/modules/markets'
import {Coin} from '@/models/coin'
import {Pair} from '@/models/pair'
import {Market} from '@/models/market'
import * as types from '../types'

const PENDING_PAIRS = 'PENDING_PAIRS';
const PENDING_COINS = 'PENDING_COINS';

const state = _.defaults({
	coins 			: [],
	pairs 			: [],
	exchanges 		: [],
	fiat 			: [],
	countries 		: [],
	totalEx 		: null,
	totalPairs		: null,
	pendingPairs 	: false,
	pendingCoins 	: false,
}, types.state);

const getters = _.defaults({
	getCoins: (state, getters, rootState ) => {
		return state.coins;
	},

	getPairs: (state, getters, rootState ) => {
		return state.pairs;
	},

	getExchanges: (state, getters, rootState ) => {
		return state.exchanges;
	},

	getFiat: (state, getters, rootState ) => {
		return state.fiat;
	},

	getCountries: (state, getters, rootState ) => {
		return state.countries.map( (v, i) => {
			return {
				'value'			: v.value,
				'text' 			: v.text
			};
		});
	},
	getTotalExchanges: state => state.totalEx,
	getTotalPairs: state => state.totalPairs,
	isPendingPairs: state => state.pendingPairs,
	isPendingCoins: state => state.pendingCoins
}, types.getters);

const actions = {
	getCoins ({ commit }, data = {} ) {
		commit(PENDING_COINS, true);
		commit(types.FAILURE, false);
		console.log('getCoins', data);

		data.isFiat = false;

		apiMarkets.getCoins(data)
			.then( (res) => {
				commit('setCoins', res.data['hydra:member']);
				commit(PENDING_COINS, false);
			})
			.catch( (err) => {
				commit(types.FAILURE, err.response.data);
				commit(PENDING_COINS, false);
			});
	},

	async getPairs ({ commit }, data ) {
		commit(PENDING_PAIRS, true);
		commit(types.FAILURE, false);
		console.log('getPairs', data);

		try{
			let res = await apiMarkets.getPairs(data);
			commit('setPairs', res.data['hydra:member']);
			commit('setTotalPairs', res.data['hydra:totalItems']);
			commit(PENDING_PAIRS, false);
		} catch ( err ) {
			commit(types.FAILURE, err.response.data);
			commit(PENDING_PAIRS, false);
			return Promise.reject( types.parseError(err) );
		}
	},

	getExchanges ({ commit }, data ) {
		commit(types.START_REQUEST);
		
		apiMarkets.getItems(data)
			.then( (res) => {
				console.log('getExchanges', res.data);
				commit('setExchanges', res.data['hydra:member']);
				commit('setTotalExchangers', res.data['hydra:totalItems']);
				commit(types.PENDING, false);
			})
			.catch( (err) => {
				commit(types.FAILURE, err.response.data);
				commit(types.PENDING, false);
			});
	}
}

const mutations = _.defaults({
	setCoins ( state, items ) {
		if ( items.length == 0 ) {
			state.coins = [];
			return;
		}
		
		state.coins = _.uniqBy(state.coins.concat(items.map( (v) => new Coin(v) )), 'id');
	},
	setPairs ( state, items ) {
		state.pairs = items.map( (v) => new Pair(v) );
	},
	setExchanges ( state, items ) {
		if ( items.length == 0 ) {
			state.exchanges = [];
			return;
		}
		
		state.exchanges = _.uniqBy(state.exchanges.concat(items.map( (v) => new Market(v) )), 'id');
	},
	setFiat ( state, items ) {
		state.fiat = items;
	},
	setCountries ( state, items ) {
		state.countries = items;
	},
	setTotalPairs ( state, v ) {
		state.totalPairs = v;
	},
	setTotalExchangers ( state, v ) {
		state.totalEx = v;
	},
	
	[PENDING_COINS] ( state, value ) {
		state.pendingCoins = value;
	},

	[PENDING_PAIRS] ( state, value ) {
		state.pendingPairs = value;
	},
}, types.mutations);

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}