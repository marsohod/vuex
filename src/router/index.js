import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import _ from 'lodash'
import store from '@/store'
import Blank from '@/components/Blank'
// Layouts
import LayoutBlank from '@/layout/LayoutBlank'

import RoutesUser from './user.js'
import RoutesAdmin from './admin.js'
import RoutesPages from './pages.js'

import {STORAGE_TOKEN} from '@/api/apiTypes'

Vue.use(Router)
Vue.use(Meta)

var RoutesCommon = [{
	path        : '/',
	redirect    : '/login',
}, {
	path        : '/exit',
	component   : Blank,
	name        : 'exit',
}, {
	path        : '/login',
	component   : LayoutBlank,
	children    : [{
		path    : '/',
		name    : 'login',
		component: () => import('@/components/Auth/Login')
	}]
}, {
	path        : '/register',
	component   : LayoutBlank,
	children    : [{
		path    : '/',
		name    : 'register',
		component: () => import('@/components/Auth/Register')
	}]
}, {
	path        : '/forgot',
	component   : LayoutBlank,
	children    : [{
		path    : '/',
		name    : 'forgot',
		component: () => import('@/components/Auth/Forgot')
	}]
}, {
	path        : '/terms',
	component   : LayoutBlank,
	name        : 'terms',
}];

var routes = RoutesCommon.concat(RoutesAdmin, RoutesUser, RoutesPages);

const router = new Router({
	base    : '/',
	mode    : 'history',
	routes,
	scrollBehavior (to, from, savedPosition) {
		if (to.hash) return { selector: to.hash };
		if (savedPosition) return savedPosition;
		
		return { x: 0, y: 0 };
	}
})

router.afterEach(() => {
	// On small screens collapse sidenav
	if (window.layoutHelpers && window.layoutHelpers.isSmallScreen() && !window.layoutHelpers.isCollapsed()) {
		setTimeout(() => window.layoutHelpers.setCollapsed(true, true), 10)
	}
})

router.beforeEach((to, from, next) => {
	// Set loading state
	document.body.classList.add('app-loading')

	if ( !localStorage.getItem(STORAGE_TOKEN) ) {
		/*if ( to.matched.some( record => record.meta.auth ) ) {
            next({name : '/login'});
        }*/
	}
    
	// Add tiny timeout to finish page transition
	setTimeout(() => next(), 10)    
})

export default router
