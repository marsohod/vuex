import axios from 'axios'

export const URL 						= `//${window.location.hostname}${window.location.port !== null ? `:${window.location.port}` : ''}/api/`;
export const STORAGE_TOKEN 				= 'token';
export const STORAGE_REFRESH_TOKEN 		= 'refresh_token';
export const STORAGE_EXPIRES_IN 		= 'expires_in';

export const API = {
	getItems ( data = {} ) {
		return axios.get(`${this.URL}.jsonld`, {params: {...data}});
	},

	getById ( id ) {
		return axios.get(`${this.URL}/${id}.jsonld`);
	},

	putItem ( data ) {
		return axios.post(`${this.URL}.json`, data);
	},

	patchItem ( data ) {
		return axios.patch(`${this.URL}/${data.id}.json`, data);
	}
}