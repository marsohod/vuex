import {User} from './user.js'
import {Comment} from './comment.js'
import {Market} from './market.js'
import {Pair} from './pair.js'
import {Term} from './term.js'
import {Rate} from './Rate.js'
import {IdeaHistory} from './ideahistory.js'
export class Idea {
	constructor ( data ) {
		if ( _.isNil(data) ) { return }
		
		_.extend(this, {
			id							: data.id,
			date 						: data.date,
			dateWithoutTimezone 		: new Date(data.date),
			dateClosed					: data.closedAt,
			dateClosedWithoutTimezone 	: data.closedAt != null ? new Date(data.closedAt) : null,
			dateBuyed					: data.buyedAt,
			dateBuyedWithoutTimezone 	: data.buyedAt != null ? new Date(data.buyedAt) : null,
			user 						: new User(data.user),
			pair 						: new Pair(data.pair),
			exchange 					: new Market(data.market),
			type						: data.type,
			horizon						: data.horizon,
			price						: data.price,
			ex_growth 					: data.upsideExpected,
			ex_profit 					: data.profitExpected != null ? Number(data.profitExpected).toFixed(2) : data.profitExpected,
			time_left 					: 0,//data.time_left,
			targets 					: data.targets ? data.targets.map( (v) => new Term(v, data.buyPrice) ) : [],
			stoploss 					: {
				price 		: data.stoplossPrice,
				percent 	: data.stoplossPercent,
				status 		: 'closed'
			},
			upside						: data.upside,
			profit 						: data.profit != null ? Number(data.profit).toFixed(2) : data.profit,
			status 						: data.status,
			closedReason 				: data.statusReason,
			time 						: null,
			case 						: data.description,
			_comments 					: data.comments ? data.comments.map((v) => new Comment(v)) : [],
			_rates 						: data.rates ? data.rates.map( (h) => new Rate(h) ) : [],
			news 						: data.news,
			history 					: data.history ? data.history : []
		});

		this._setTime();
		this._setMaxDecimalForPrices();

		//TMP
		// this.history = [
		// 	new IdeaHistory({id: 8, type: 'stoploss', price: 6000, date: '2018-08-12T20:00:00+00:00'})
		// ]
	}

	get comments() {
		return this._comments;
	}

	set comments(value) {
		this._comments = [];
		_.each(value, (v) => this.addComment(v) );
	}

	addComment ( c, unshift = false ) {
		if ( unshift ) {
			this._comments.unshift( c instanceof Comment ? c : new Comment(c) );
			return;
		}

		this._comments.push( c instanceof Comment ? c : new Comment(c) );
	}

	get rates() {
		return this._rates;
	}

	set rates(value) {
		this._rates = [];

		_.each(value, (v) => this._rates.push( v instanceof Rate ? v : new Rate(v) ) );

		if ( this.rates.length > 0 ) {
			_.each(this.targets, (v, i) => this.targets[i].setAchieved(_.last(this.rates).close, this.getPrice()) );
			this._setStoplossAchieved(_.last(this.rates).close);
		}
	}

	separateHistory () {
		let b = [];
		let a = [];
		let closedAt	= this.dateClosed != null ? new Date(this.dateClosed) : null;
		let createdAt 	= new Date(this.date);

		_.each( this.rates, (h) => {
			let d = new Date(h.date);

			if ( d <= createdAt ) {
				b.push(h);
				return;
			}

			if ( closedAt == null || d <= closedAt ) { a.push(h); }
		});

		return {ratesBeforeDateCreate: b, ratesAfterDateCreate :a};
	}
	
	getBuyedCoin () { return this.pair.coinFrom; }
	getPaidCoin () { return this.pair.coinTo; }
	getPrice () { return this.price; }

	// STATUSES
	isActive () {
		return this.status === IDEA_STATUSES[0].id;
	}

	wasSold () {
		return !_.isNil(this.closedReason) && this.closedReason === IDEA_REASONS[0].id;
	}

	wasBought () {
		return this.isBuyed() || this.dateBuyed != null;
	}

	isBuyed () {
		return this.status === IDEA_STATUSES[2].id;
	}

	isClosed () {
		return this.status === IDEA_STATUSES[1].id;
	}

	isExit () {
		return this.closedReason === IDEA_REASONS[2].id;
	}

	_setTime () {
		let days 	= 1000 * 3600 * 24;
		let now 	= this.isClosed() ? new Date(this.dateClosed) : new Date();

		this.time = Math.abs( Math.ceil((Math.abs((new Date(this.date)).getTime() - now.getTime())) / days) );
	}

	_setStoplossAchieved ( currentPrice ) {
		let spread = currentPrice - this.getPrice();

		if ( !this.wasBought() ||
			(this.stoploss.percent < 0 && spread > 0) ||
			(this.stoploss.percent > 0 && spread < 0) ) {
			
			this.stoploss.achieved = 0;
			return;
		}

		this.stoploss.achieved = Math.abs(this.stoploss.percent * (this.getPrice() - currentPrice) / (this.stoploss.price - this.getPrice()));
		this.stoploss.achieved = Number((this.stoploss.achieved > this.stoploss.percent ? this.stoploss.percent : this.stoploss.achieved)).toFixed(2);
	}

	/* TYPE */
	isWatch () {
		return this.type == IDEA_TYPES[2].id || this.targets.length == 0;
	}

	isHold () {
		return this.type == IDEA_TYPES[1].id;
	}

	isSell () {
		return this.type == IDEA_TYPES[0].id;
	}
	/* END TYPE */

	/* HORIZON */
	isShort () {
		return this.horizon == IDEA_HORIZONS[0].id;
	}

	isMid () {
		return this.horizon == IDEA_HORIZONS[1].id;
	}

	isLong () {
		return this.horizon == IDEA_HORIZONS[2].id;
	}
	/* END HORIZON */

	calculateProfit () {
		return 1;
	}

	_setMaxDecimalForPrices () {
		let amount = this.maxDecimalLength();

		this.price 			= Number(this.price).toFixed(amount);
		this.stoploss.price = this.stoploss.price != null ? Number(this.stoploss.price).toFixed(amount) : this.stoploss.price;
		_.each( this.targets, (v,i) => this.targets[i].price = Number(this.targets[i].price).toFixed(amount) );
	}

	maxDecimalLength () {
		let amount = _.max( _.map(this.targets, v => Number.prototype.getDecimal(v.price)).concat(
			Number.prototype.getDecimal(this.stoploss.price),
			Number.prototype.getDecimal(this.price)
		));

		if ( this.pair.coinTo.symbol.match(/USD/) == null ) {
			amount = amount > 2 ? 8 : amount;
		}

		return amount;
	}
}

export const IDEA_STATUSES = [
	{
		id 		: 'ACTIVE',
		name 	: 'Active'
	},
	{
		id 		: 'CLOSED',
		name 	: 'Closed'
	},
	{
		id 		: 'BUYED',
		name 	: 'Buyed'
	}
]
export const IDEA_REASONS = [
	{
		id 		: 'STOPLOSS',
		name 	: 'Stoploss'
	},
	{
		id 		: 'SUCCESS',
		name 	: 'Success'
	},
	{
		id 		: 'EXIT',
		name 	: 'Exit'
	}
];

export const IDEA_TYPES = [
	{ id: 'TRADE', title: 'Trade' },
	{ id: 'HOLD', title: 'Hold' },
	{ id: 'WATCH', title: 'Watch' },
]

export const IDEA_HORIZONS = [
	{ id: 'SHORT', title: 'Short' },
	{ id: 'MID', title: 'Mid' },
	{ id: 'LONG', title: 'Long' }
]