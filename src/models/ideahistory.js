const HISTORY_STOPLOSS 	= 'stoploss';
const HISTORY_TARGET 	= 'target';

export class IdeaHistory {
	constructor ( data ) {
		if ( !data || data == null ) { return; }
		
		_.extend(this, {
			id  	: data.id,
			type 	: data.type,
			price	: data.price,
			date 	: data.date
		});
	}

	isStoploss () {
		return this.type == HISTORY_STOPLOSS;
	}

	isTarget () {
		return this.type == HISTORY_TARGET;
	}
}