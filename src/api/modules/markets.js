import axios from 'axios'
import * as api from '../apiTypes'
import _ from 'lodash'

export default _.defaults({
	URL: `${api.URL}markets`,
	URLCoins: `${api.URL}coins`,

	getCoins ( data ) {
		return axios.get(`${this.URLCoins}.jsonld`, {params: data});
	},

	getPairs ( data ) {
		return axios.get(`${api.URL}pairs.jsonld`, {params: data});
	},

	getMarketPairPrice ( data ) {
		return axios.get(`${api.URL}util/price.json`, {params: data});
	},

}, api.API);