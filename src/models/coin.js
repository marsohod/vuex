export class Coin {
	constructor ( data ) {
		if ( !data ) { return; }
		
		_.extend(this, {
			id  	: data.id,
			symbol	: data.symbol,
			name	: data.name,
			rank	: data.rank ? data.rank : null,
			isFiat	: data.isFiat,
			icon	: data.icon == null ? 'https://s2.coinmarketcap.com/static/img/coins/128x128/1.png' : data.icon.contentUrl
		});
	}
}