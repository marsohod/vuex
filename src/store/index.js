import Vue from 'vue'
import Vuex from 'vuex'
import ideas from './modules/ideas'
import market from './modules/market'
import users from './modules/users'

Vue.use(Vuex);

export default new Vuex.Store({
	strict 	: process.env.NODE_ENV !== 'production',
	modules 	: {
		users,
		ideas,
		market
	}
})