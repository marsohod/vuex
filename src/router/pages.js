import Layout from '@/layout/LayoutWithoutSidenav'

export default [
	{
		path        : '/faq',
		component   : Layout,
		children    : [{
			path    : '/',
			name    : 'faq',
			component: () => import('@/components/Faq')
		}]
	}
]