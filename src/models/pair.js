import {Coin} from './coin.js'

export class Pair {
	constructor ( data ) {
		if ( !data ) { return; }
		
		_.extend(this, {
			id 		: data.id,
			symbol	: data.symbol,
			coinFrom: new Coin(data.coinFrom),
			coinTo	: new Coin(data.coinTo),
		});
	}
}