export default {
	props 	: {
		showPaginationTop	: false,
		hideSearch 			: false,
		hidePerPage 		: false,
		hidePagination		: false,
		hideInfo 			: false,
		filters 			: {}
	},
	data () {
		return {
			sortBy 			: 'id',
			sortDesc 		: true,
			perPage 		: 25,
			fields 			: [],
			currentPage 	: 1,
			fixed 			: false,
			striped			: true,
			hover			: false,
			bordered 		: true,
			showempty		: false,
			noLocalSorting 	: false,
			sorting			: false,
			rowclick		: () => {},
			isbusy			: true,
		}
	},
	computed : {
		totalPages () {
			return Math.ceil(this.totalItems / this.perPage)
		}
	},
	methods : {
		filter (value) {
			const val 		= value.toLowerCase()
			const filtered 	= this.originalUsersData.filter(d => {
				return Object.keys(d)
					.filter(k => this.searchKeys.includes(k))
					.map(k => String(d[k]))
					.join('|')
					.toLowerCase()
					.indexOf(val) !== -1 || !val
			})
			this.usersData = filtered
		},

		pageLengthChanged (v) {
			this.perPage = v
			this.sorting(this);
		},

		pageChanged (v) {
			this.currentPage = v
			this.sorting(this);
		},
		searching () {
			return _.debounce( () => {	
				this.sorting(this);
			}, 400);
		}
	}
}