Number.prototype.isFloat = function (v) {
	return Number(v) % 1 != 0;
}

Number.prototype.getDecimal = function (v) {
	if ( !Number.prototype.isFloat(v) ) { return 0; }
	
	return String(v).split('.')[1].length;
}