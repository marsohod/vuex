import Layout from '@/layout/Layout2'

import Blank from '@/components/Blank'

export default [{
	path 		: '/admin',
	redirect 	: '/admin/ideas',
	component 	: Layout,
	meta 		: {auth: true},
	children: [{
		path        : 'ideas',
		component 	: () => import('@/components/Ideas'),
		name        : 'ideas',
	}, {
		path        : 'ideas/create',
		component 	: () => import('@/components/NewIdea'),
		name        : 'newidea'
	}, {
		path        : 'ideas/:id',
		component 	: () => import('@/components/Idea'),
		name        : 'idea'
	}, {
		path        : 'people',
		component 	: () => import('@/components/People'),
		name        : 'people'
	}, {
		path        : 'exchanges',
		component 	: () => import('@/components/Exchanges'),
		name        : 'exchanges'
	}, {
		path        : ':username',
		component 	: () => import('@/components/Profile'),
		name        : 'profile',
	}, {
		path        : ':username/ideas/:id',
		component 	: () => import('@/components/Idea'),
		name        : 'profile_idea',
		params 		: { user: true },
		meta 		: {
			profile : true
		}
	}]
}]