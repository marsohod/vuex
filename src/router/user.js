import LayoutUser from '@/layout/Layout2User'
import Blank from '@/components/Blank'

export default [{
	path 		: '/user',
	name 		: 'user_',
	redirect 	: '/user/people',
	component 	: LayoutUser,
	meta 		: {auth: true},
	children 	: [{
		path		: 'ideas',
		component 	: () => import('@/components/UserIdeas'),
		name		: 'user_ideas'
	}
}]