String.prototype[Symbol.for('isEmail')] = function () {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( this );
}

String.prototype[Symbol.for('isUrl')] = function () {
    return /(http|ftp|https):\/\/[а-я\w-]+(\.[а-я\w-]+)+([а-я\w.,@?^=%&amp;:\/~+#-]*[а-я\w@?^=%&amp;\/~+#-])?/i.test( this );
}

String.prototype[Symbol.for('isValidHttpUrl')] = function () {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-zа-я\\d]([a-zа-я\\d-]*[a-zа-я\\d])*)\\.?)+[a-zа-я]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    return pattern.test( this );
}

String.prototype[Symbol.for('hasProtocol')] = function () {
    return /(http|ftp|https):\/\//.test( this );
}

String.prototype[Symbol.for('isPasswordWeak')] = function () {
    return ( this.length < 6 || this.length > 200 );   
}