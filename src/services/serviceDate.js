Date.prototype.sameDay = function(d) {
	return this.getFullYear() === d.getFullYear()
		&& this.getDate() === d.getDate()
		&& this.getMonth() === d.getMonth();
}

Date.prototype.timeAgo = function( secondToEvent ) {
	var min     = 60;
	var hour    = min * 60;
	var day     = hour * 24;
	var month   = day * 30;
	var year    = month * 12;

	if ( secondToEvent / year >= 1 ) {
		let y = Math.round( secondToEvent / year );
		return `${y} year${y == 1 ? '' : 's'}`;
	}
	if ( secondToEvent / month >= 1) {
		let m = Math.round( secondToEvent / month );
		return `${m} month${m == 1 ? '' : 's'}`;
	}
	if ( secondToEvent / day >= 1) {
		let m = Math.round( secondToEvent / day );
		return `${m} day${m == 1 ? '' : 's'}`;
	} 
	if ( secondToEvent / hour >= 1) {
		let m = Math.round( secondToEvent / hour );
		return `${m} hour${m == 1 ? '' : 's'}`;
	}
	if ( secondToEvent / min >= 1) {
		let m = Math.round( secondToEvent / min );
		return `${m} minute${m == 1 ? '' : 's'}`;
	}

	let m = Math.round( secondToEvent );

	return `${m} second${m == 1 ? '' : 's'}`;
}