export class Statistic {
	constructor ( data ) {
		if ( !data ) { return; }
		
		_.extend(this, {
			date  		: data.date,
			open		: data.open,
			closed		: data.closed,
			returned	: data.returned,
			ex_return	: data.ex_return,
			bgci		: data.bgci
		});
	}
}