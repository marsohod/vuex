import axios from 'axios'
import * as api from '../apiTypes'
import _ from 'lodash'

export default _.defaults({
	URL: `${api.URL}users`
}, api.API);