import {User} from './user.js'
import D from '@/services/serviceDate'

export class Comment {
	constructor ( data ) {
		if ( _.isNil(data) ) { return }
		
		_.assignIn(this, {
			id				: data.id,
			date 			: data.createdAt,
			author 			: new User(data.author),
			body 			: data.body,
			ago 			: Date.prototype.timeAgo(((new Date()).getTime() - new Date(data.createdAt).getTime())/1000)
		});
	}
}