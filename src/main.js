import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate';
import store from './store'

import globals from './globals'
import Popper from 'popper.js'
import _ from 'lodash'
import moment from 'moment'
import S from './services/serviceString'
import D from './services/serviceDate'
import N from './services/serviceNumber'
import Notifications from 'vue-notification'

// Required to enable animations on dropdowns/tooltips/popovers
Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});
Vue.use(Notifications);

// Global RTL flag
Vue.mixin({
	data: globals
})

/* eslint-disable no-new */
export const bus = new Vue({
	el 			: '#app',
	router,
	store,
	render: h => h(App)
	// template 	: '<App/>',
	// components	: { App }
})


/* FILTERS FOR USE EVARYWHERE */
Vue.filter('capitalize', function (value) {
	if (!value) return '';
	
	value = value.toString();
	return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
});

Vue.filter('uppercase', function (value) {
	if (!value) return '';

	return (value+'').toUpperCase();
});

Vue.filter('dateMMMDDYYYY', function (value) {
	if (!value) return '';
	
	return moment( new Date(value) ).format('MMM DD, YYYY');
});

Vue.filter('dateMMSDDSYYYY', function (value) {
	if (!value) return '';
	
	return moment( new Date(value) ).format('MM/DD/YYYY');
});

Vue.filter('dateMMYYYY', function (value) {
	if (!value) return '';
	
	return moment( new Date(value) ).format('MMM YYYY');
});

Vue.filter('dateDDMMMYYYY', function(value) {
	if (value) {
		return moment(String(value)).format('DD MMM YYYY')
	}
});

Vue.filter('dateDDMMMMYYYY', function(value) {
	if (value) {
		return moment(String(value)).format('DD MMMM YYYY')
	}
});

Vue.filter('dateDDMMMMYYYYHHmm', function(value) {
	if (value) {
		return moment(value).format('DD MMMM YYYY HH:mm')
	}
});
/* END FILTERS FOR USE EVARYWHERE */