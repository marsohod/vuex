export class Knowledge {
	constructor ( data ) {
		if ( !data ) { return; }
		
		_.extend(this, {
			id  	: data.id,
			// href	: data.open,
			name	: data.name,
			text	: data.text
		});
	}
}