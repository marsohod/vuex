import axios from 'axios'
import * as api from '../apiTypes'
import _ from 'lodash'

export default _.defaults({
	URL			: `${api.URL}ideas`,
	URLTargets 	: `${api.URL}targets`,

	getRates ( data ) {
		return axios.get(`${api.URL}rates.json`, {params: data});
	},

	exit (data) {
		return axios.put(`${this.URL}/${data.id}/exit.json`);
	},

	putTarget ( data ) {
		return axios.post(`${this.URLTargets}.json`, data);
	},

	patchTarget ( data ) {
		return axios.patch(`${this.URLTargets}/${data.id}.json`, data);
	},

	getResults ( data ) {
		if ( data.user_id ) {
			let uid = data.user_id;
			delete data.user_id;

			return axios.get(`${api.URL}users/${uid}/results.json`, {params: data});
		}

		return axios.get(`${api.URL}results.json`, {params: data});
	},

	getPerformance ( data ) {
		return axios.get(`${api.URL}index/performance.json`, {params: data});
	},

	getIndexStat ( data ) {
		return axios.get(`${api.URL}util/indexstat.json`, {params: data});
	},

	putToWatchlist ( data ) {
		return axios.post(`${this.URL}/watchlist.json`, data);
	}
}, api.API);