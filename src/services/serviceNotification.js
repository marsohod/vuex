const GROUP = 'notifications-default';

export default {
	error ( text, title = 'Something wrong!') {
		this.$notify({
			group	: GROUP,
			type	: 'bg-danger text-white',
			title	: title,
			text	: text
		});
	},

	success ( text, title = 'Success!') {
		this.$notify({
			group	: GROUP,
			type	: 'bg-success text-white',
			title	: title,
			text	: text
		});
	}
}