export class Term {
	constructor ( data, buyPrice ) {
		if ( !data ) { return; }
		
		_.extend(this, {
			id 							: data.id,
			dateClosed					: data.closedAt,
			dateClosedWithoutTimezone 	: data.closedAt != null ? new Date(data.closedAt.replace('+00:00', '')) : null,
			amount						: data.amount,
			price						: data.price,
			upside 						: data.upside,
			upsideExpected				: data.upsideExpected, 
			profit 						: data.profit,
			percent 					: !buyPrice ? null : data.price/buyPrice,
			status 						: data.status,
			achieved 					: 0
		});
	}

	isClosed () {
		return this.status == 'CLOSED';
	}

	setAchieved ( currentPrice, buyPrice ) {
		let ups = this.upside;

		if ( this.upside < 0 ) {
			if ( currentPrice > buyPrice ) {
				this.achieved = 0;
				return;
			}

			ups = -ups;
		}	

		this.achieved = ups * (currentPrice - buyPrice) / (this.price - buyPrice);
		this.achieved = Number((this.achieved < 0 ? 0 : (this.achieved > this.upside ? this.upside : this.achieved))).toFixed(2);
	}
}