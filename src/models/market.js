import {Country} from './country.js'
import {Pair} from './pair.js'
import {Coin} from './coin.js'

export class Market {
	constructor ( data ) {
		if ( !data ) { return; }

		_.extend(this, {
			id  		: data.id,
			name		: data.name,
			rating		: data.rating,
			country		: data.country != null ? new Country(data.country) : null,
			coins		: data.coins ? data.coins.map( (v) => new Coin(v) ) : [],
			pairs 		: data.pairs ? data.pairs.map( (v) => new Pair(v) ) : [],
			// pair: data.pair.map( (v) => new Coin(v) ),
			trades		: data.trades,
			volume		: data.volume,
			marketshare	: data.marketshare,
			site		: data.website
		});
	}
}
